require 'appium_lib'
require 'rspec/expectations'

include RSpec::Matchers

capabilities = {
    caps:{
        deviceName: "emulator-5554",
        platformName: "Android",
        platformVersion: "#{ENV['platformVersion']}",
        app: (File.join(File.dirname(__FILE__), "morelocale-2-2-4-5.apk")),
        appPackage: "jp.co.c_lis.ccl.morelocale",
        # appActivity: "jp.co.c_lis.ccl.morelocale.ui.MainActivity"
        appWaitActivity: "*",
        },
        appium_lib:{
            # server_url: "http://0.0.0.0:4723/wd/hub",
            wait: 30
        }
    }

@driver = Appium::Driver.new(capabilities, true)
@touch = Appium::TouchAction.new(@driver)
Appium.promote_appium_methods Object