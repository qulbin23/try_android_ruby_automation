@Edit_Locale
Feature: Edit Locale

@Edit_iso_locale
Scenario: Edit locale from ISO list
Given user launch the AUT
Then User tap on a locale option
And User tap on Edit
Then User edit label
And User add locale from ISO list

@Edit_manual_locale
Scenario: Edit manual locale
Given user launch the AUT
Then User tap on a locale option
And User tap on Edit
Then User edit label
And User edit manual locale